###############################
VectorLinux repo monitor daemon
###############################


This is a Twisted/INotify daemon for listening to changes in the repository
filesystem tree. Repository metadata needs to be updated each time a change is
made in a package. With the repository monitor we can automate these tasks.


Install
=======

    $ python setup.py install


Running
=======

The install rutine will put service scripts in /etc/rc.d/rc.repomonitor

    $ /etc/rc.d/rc.repomonitor {start|stop|restart}

The repository location is read from the `VL_REPOSITORY_ROOT` environment
variable, which should be an absolute path.


Testing
=======

We use Twisted's Trial module for tests, from the source top directory run

    $ trial repomonitor
