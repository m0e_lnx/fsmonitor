# coding: utf8


import os
import repomonitor
from twisted.application import service, internet
from twisted.python.log import ILogObserver, FileLogObserver
from twisted.python.logfile import DailyLogFile


repomonitor.main()
application = service.Application("VL-RepoMonitor")
logfile = DailyLogFile("repomonitor.log", "/tmp")
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)
