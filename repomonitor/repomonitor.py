# coding: utf8

"""Watch the root of a repo and run a callback

"""


import os, sys, subprocess
from .inotify import *


REPO_ROOT = os.environ["VL_REPOSITORY_ROOT"]
PACKAGE_EXTENSIONS = (".txz", ".tlz", ".tbz", ".tgz")
META_EXTENSIONS = (".meta", ".info")


class RepositoryINotifyHandler(INotifyEventHandler):
    """Watch a repository root for fs changes

    """
    def on_create(self, filepath, mask):
        if ispackage(filepath):
            log.msg("Package written: %s" % filepath)
        elif ismeta(filepath):
            log.msg("Metadata file created: %s" % filepath)

    def on_modify(self, filepath, mask):
        if ispackagestxt(filepath):
            log.msg("PACKAGES.TXT updated")


def update_repository():
    """Run the repository scripts with subprocess

    """
    script = "/usr/libexec/openssh/sftp-server_repo-wrapper"
    try:
        subprocess.check_call(script)
    except:
        log.err()


def ispackage(filepath):
    """Return True if filepath is a path to a package

    """
    file_extension = os.path.splitext(filepath.path)[1]
    return file_extension in PACKAGE_EXTENSIONS


def ismeta(filepath):
    """Return True if filepath is a .meta

    """
    file_extension = os.path.splitext(filepath.path)[1]
    return file_extension in META_EXTENSIONS


def ispackagestxt(filepath):
    """Return True if filepath is PACKAGES.TXT

    """
    return filepath.path.endswith("PACKAGES.TXT")


def main():
    """Create a handler and register it

    """
    handler = RepositoryINotifyHandler(REPO_ROOT)
    handler.register()
    return handler
