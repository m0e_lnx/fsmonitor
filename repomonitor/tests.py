# coding: utf8


"""Tests for repowatcher

"""


from twisted.trial import unittest
from twisted.python import filepath
from twisted.internet import defer
from .inotify import INotifyEventHandler
import os


class INotifyEventHandlerTest(unittest.TestCase):

    def setUp(self):
        self.tests_dir = filepath.FilePath(self.mktemp())
        self.dir = self.tests_dir.path
        self.tests_dir.createDirectory()
        self.handler = None

    def tearDown(self):
        self.tests_dir.remove()
        self.handler.unregister()

    @defer.inlineCallbacks
    def testEventHandler(self):

        d1 = defer.Deferred()
        d2 = defer.Deferred()

        class INotifyHandler(INotifyEventHandler):

            called = []

            def on_create(self, filepath, mask):
                self.called.append("on_create")
                d1.callback(1)

            def on_modify(self, filepath, mask):
                self.called.append("on_modify")
                d2.callback(2)

        self.handler = INotifyHandler(self.dir)
        self.handler.register()
        with open(os.path.join(self.dir, "test.txt"), "w") as f:
            f.write("test")
        yield d1
        yield d2
        self.assertEqual(self.handler.called, ['on_create', 'on_modify'])

